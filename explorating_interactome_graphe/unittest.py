import unittest
import ProjeyPython

class FunctionTest(unittest.TestCase):
    
    """function test_interaction_file_dict
       test if the result of the function read_interaction_file_dict is of type dict
    """
    def test_interaction_file_dict(self):
        file1="graphe.txt"
        dict_type=type(ProjeyPython.read_interaction_file_dict(file1))
        self.assertEqual(dict_type, dict)
        
    """function test_interaction_file_list
       test if the result of the function read_interaction_file_list is of type list
    """    
    def test_interaction_file_list(self):
        file1="graphe.txt"
        list_type=ProjeyPython.read_interaction_file_list(file1)
        self.assertEqual(type(list_type), list)
    
    """function test_interaction
       test if the result of the function read_interaction_file is of type tuple
    """
    def test_interaction(self):
        file1="graphe.txt"
        tuple_type=ProjeyPython.read_interaction_file(file1)
        self.assertEqual(type(tuple_type), tuple)

    def test_count_vertices(self):
        """
        This function tests the count_vertices function.

        Returns
        -------
        int number of vertices

        """
        print("\n")
        ProjeyPython.count_vertices("Human_HighQuality.txt")
        ProjeyPython.count_vertices("toy_example.txt")#expected result 6
        ProjeyPython.count_vertices("graphe_with_repetition.txt")#expected result 8
        ProjeyPython.count_vertices("fichier_vide.txt")#expected result 0
    def test_count_edges(self):
        """
        This function tests the count_edges function.

        Returns
        -------
        int number of edges. Number of edges corresponding at the number of interaction

        """
        print("\n")
        ProjeyPython.count_edges("Human_HighQuality.txt")#expected result 27276 
        ProjeyPython.count_edges("toy_example.txt")#expected result 6
        ProjeyPython.count_edges("graphe_with_repetition.txt")# expected result 8 
        ProjeyPython.count_edges("fichier_vide.txt")# expected result 0
    def test_clean_interactome(self):
        """
        This function will allow to test the clean_interact function, it removes redundant interactions, 
        and all the homo-dimer proteins.

        Returns
        -------
        New interactome file Cleaning

        """
        ProjeyPython.clean_interactome("toy_example.txt")
        ProjeyPython.clean_interactome("graphe_with_repetition.txt")
        ProjeyPython.count_vertices("toy_example_Cleaning.txt")#result expected 6
        ProjeyPython.count_vertices("graphe_with_repetition_Cleaning.txt")# result expected 8
        ProjeyPython.count_edges("toy_example_Cleaning.txt")#result expected 6
        ProjeyPython.count_edges("graphe_with_repetition_Cleaning.txt")# result expected 6
        ProjeyPython.is_interaction_file("toy_example_Cleaning.txt")#expected result true
        ProjeyPython.is_interaction_file("graphe_with_repetition_Cleaning.txt")# the output file is interactome file 
        origin_file=(type("toy_example.txt"))
        cleaning_file=(type("toy_example_cleaning.txt"))
        self.assertEqual(origin_file(), cleaning_file())#the type of imput file is equal the type of output file
    def test_get_degree(self):
        """
        This function test get_degree function

        Returns
        -------
         int The degree of protein

        """
        ProjeyPython.get_degree("toy_example.txt", "D")#result expected 3
        ProjeyPython.get_degree("graphe_with_repetition.txt", "A")# result expected 3
        ProjeyPython.get_degree("graphe_with_repetition_Cleaning.txt", "A")# result expected 2
    def test_get_max_degree(self):
        """
        This function test get_max_degree function

        Returns
        -------
        a dict , values with the highest max. degree and the max. de

        """
        ProjeyPython.get_max_degree("graphe_with_repetition.txt") #result expected 4 with prot B
        ProjeyPython.get_max_degree("graphe_with_repetition_Cleaning.txt")#result expected 2 with prot A,B,C,E
        ProjeyPython.get_max_degree("toy_example.txt")#result expected 3 with prot B,D
        ProjeyPython.get_max_degree("toy_example_Cleaning.txt")#result expected 3 with prot B,D
       
    def test_get_ave_degree(self):  
        """
         This function test get_ave_degree function

        Returns
        -------
        the intaverage degree

        """
        ProjeyPython.get_ave_degree("graphe_with_repetition.txt")   # result expected 2
        ProjeyPython.get_ave_degree("graphe_with_repetition_Cleaning.txt") # result expected 1.5
        ProjeyPython.get_ave_degree("toy_example.txt")   #result expected 2.0
        ProjeyPython.get_ave_degree("toy_example_Cleaning.txt") #result expected 2.0
    def test_count_degree(self):
        """
        
       This function test count_degree function
        Returns
        -------
        int the number of prot with a degree determined

        """
        ProjeyPython.count_degree("toy_example.txt",3)#result expected 2
        ProjeyPython.count_degree("graphe_with_repetition.txt",3) # result expected 1  
        ProjeyPython.count_degree("graphe_with_repetition_Cleaning.txt",3) #result expected 0
    def test_histogramme_degree(self):
        """
        This function test histogramme degree

        Returns
        -------
        the number of proteins with a degreed between two limits

        """
        ProjeyPython.histogramme_degree("toy_example.txt",1,3)#all 2*
        ProjeyPython.histogramme_degree("graphe_with_repetition_Cleaning.txt",1,3) # result expected 4*,4* 0*
        ProjeyPython.histogramme_degree("graphe_with_repetition.txt",2,4) #result expected 3* 1* 4*
        #ProjeyPython.histogramme_degree("Human_HighQuality.txt",2,6)


unittest.main()
