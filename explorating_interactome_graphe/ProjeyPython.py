import os.path

######Questions preliminaires:######

#variable for the path to the directory that contains files


##1-2-1##

"""function read_interaction_file_dict
   argts:the file to read
   allows to read a tabulated file and store the two elements of each line in a dictionary by skipping the first line
"""
def read_interaction_file_dict(file):
    #read the file by using a wrapper
    with open(file, "r") as graphe_interactome:
        #initialize the dictionary
        dict_interaction={}
        #store the first line in a variable to skip it
        str_first_line=graphe_interactome.readline()
        #loop for to browse the lines of the file
        for line in graphe_interactome.readlines():
            
            #separate the line into a list containing as elements each word of the line which are separated by tabs
            list_word=line.split()
            #make sure that the line does not exceed 2 elements (words)
            if len(list_word) == 2:
                #if prot1 is not a key 
                if list_word[0] not in dict_interaction.keys() :
                    #prot1 becomes a key
                    dict_interaction[list_word[0]] = []
                    #the second element of the line the value corresponding to the key
                    dict_interaction[list_word[0]].append(list_word[1])
                else:
                    #otherwise create a new key with its associated value
                    dict_interaction[list_word[0]].append(list_word[1])
                if list_word[1] not in dict_interaction.keys() :
                    #prot2 becomes a key
                    dict_interaction[list_word[1]] = []
                    #the second element of the line the value corresponding to the key
                    dict_interaction[list_word[1]].append(list_word[0])
                    #otherwise if prot2 is already a key
                else:
                    #the second element of the line the value corresponding to the key
                    dict_interaction[list_word[1]].append(list_word[0])   
    return dict_interaction
    

##1-2-2##
"""function read_interaction_file_list
   argts:the file to read
   allows to read a tabulated file and store the two elements of each line in a list by skipping the first line
"""
def read_interaction_file_list(file):
    #read the file by using a wrapper
    with open(file, "r") as graphe_interactome:
        #initialize the list
        list_interaction=[]
        #store the first line in a variable to skip it
        str_first_line=graphe_interactome.readline()
        #loop for to browse the lines of the file
        for line in graphe_interactome.readlines():
            #delete '\n' to not display it with print
            #str_new_line=line.replace("\n","")
            #separate the line into a list containing as elements each word of the line which are separated by tabs
            list_word=line.split()
            #make sure that the line does not exceed 2 elements (words)
            if len(list_word) == 2:
                #add the protein couple to the list
                list_interaction=list_interaction+[(list_word[0],list_word[1])] 
    return list_interaction

            
#####1-2-4############
"""function read_interaction_file
   argts:the file to read
   allows to read a tabulated file and store the two elements of each line in a tuple of dictionary and a list by skipping the first line
   returns a couple composed of a dictionary and a list
"""
def read_interaction_file(file):
        #call the first function and store its result in a variable
        d_int=read_interaction_file_dict(file)
        #call the second function and store its result in a variable
        l_int=read_interaction_file_list(file)
        #sotre the tuple composed of a dictionary and a list in a variable
        dict_list=(d_int,l_int) 
        return dict_list

#####1-2-7############
"""function is_interaction_file
   argts:the file to read
   allows to read a tabulated file and check if it's a good file by performing several tests
   returns true if all the tests are good or false if the file does'nt answer the tests
"""
def is_interaction_file(file):
    #read the file by using a wrapper
    with open(file, "r") as path_to_file_TextIOWrapper:
        #store the first line in a variable
        str_first_line=path_to_file_TextIOWrapper.readline()
        str_first_line=str_first_line.replace("\n","")
        #check that the first line (and file) is not empty
        if str_first_line != "" :
            #counter to count the number of lines in the file    
            int_total_lines=0
            #loop for to browse the lines of the file
            for line in path_to_file_TextIOWrapper:
                #increment the counter by 1
                int_total_lines+=1
                #separate the line into a list containing as elements each word of the line which are separated by tabs
                list_word=line.split('\t')
                #check if the line contains only 2 elements (words)
                if len(list_word) == 2 :
                    str_answer='True'
                else:
                    str_answer='False'
            #check that the first line contains the number of interactions in the file
            if str_first_line == str(int_total_lines):
                str_answer='True'
            else:
                str_answer='False'
        else:
            str_answer='False'
        return (str_answer)

def count_vertices(file):
    """
    

    Parameters
    ----------
    file : TextIOWrapper
        This function returns the number of vertices of an interaction graph file.

    Returns
    -------
    nb_vertices_int : int
       the number of vertices

    """
    #the number of vertices in an interaction file corresponds to the number of keys
    #we return the number of keys
    nb_vertices_int=len(read_interaction_file_dict(file))
    #the number of vertices is displayed
    print("Number of vertices : ", nb_vertices_int)
    return nb_vertices_int

def count_edges(file):
    """
    

    Parameters
    ----------
    file : TextIOWrapper
       This function returns the number of edges of an interaction graph file.

    Returns
    -------
    nb_edges_int : int
        the number of edges

    """
    #the number of edges corresponds to the number of interaction or, 
    #in a list, a line corresponds to an interaction.
    nb_edges_int=len(read_interaction_file_list(file))
    print("Number of edges: ", nb_edges_int)
    return nb_edges_int

def clean_interactome(file) :
    """
    

    Parameters
    ----------
    file : TextIOWrapper
        takes as input a file containing information repetitions of the type redundant interactions ( A-C then C-A) 
        or homodimeric interactions ( A, A)

    Returns
    -------
    This function returns a new cleaned up interaction file

    """
  
    interaction_for_cleaning_list = read_interaction_file_list(file)
    list_interactions = []
    for interaction_couple in interaction_for_cleaning_list  :
        interaction_couple_2 = (interaction_couple[1], interaction_couple[0])
        # control redondance
        if interaction_couple not in list_interactions :
            if interaction_couple_2 not in list_interactions :
                #control homodimeric interaction
                if interaction_couple[0] != interaction_couple[1]:
                    list_interactions.append(interaction_couple)
    number_interactions = str(len(list_interactions))
    #recuperate the nane of file 
    name_file=file.split(".")
    #create new file the name his name is his former name followed by the prefix Cleaning
    with open(name_file[0] +"_Cleaning.txt", "w") as interatome_file_cleaning:
        #write the first line 
        interatome_file_cleaning.write(str(number_interactions + '\n'))
        
        for interaction_couple in list_interactions :
            new_interaction = interaction_couple[0], interaction_couple[1]
            #each prot at separate by space
            new_interaction_bis = ' '.join(new_interaction)
            #at the end of each line a return to the line is added
            interatome_file_cleaning.write(str(new_interaction_bis + '\n'))

def get_degree(file, prot) :
    """
    

    Parameters
    ----------
    file : TextIOWrapper
        interactome file.
    prot : str
        The name of proteine contain in interacome file

    Returns
    -------
    number_interactions : int
        The degree of referenced protein

    """
    interaction_dict = read_interaction_file_dict(file)
    #for each key
    for protein in interaction_dict.keys() :
        #verify imput prot is a keys 
        if prot == protein :
            #mesure number of values of keys
             number_interactions = len(interaction_dict[protein])
    #print("Le degres de la protéine" ,prot , "est de " , number_interactions)
    return number_interactions


def get_max_degree(file) :
    """
    This function calculates the maximum degree and displays the proteins that have this maximum degree as well as the value of this maximum degree.

    Parameters
    ----------
    file : TextIOWrapper
        interactome file

    Returns
    -------
    result : list
        The name of prot whiytch the max degree and max degree

    """
    
    interaction_dict = read_interaction_file_dict(file)
    vertices_degree_dict={}
    for key in interaction_dict.keys() :
        #create new dictionnary
       vertices_degree_dict[key] = []
       #key is prot , value is degree
       vertices_degree_dict[key].append(len(interaction_dict[key]))  
       #create a list with unique prot with max degree
    key_list = [k  for (k, val) in vertices_degree_dict.items() if val== max(vertices_degree_dict.values())]
    # the result is composed prot whitch max degree , and max degree
    result=key_list, max(vertices_degree_dict.values())
    print(result)
    return result
def get_ave_degree(file):
    """
    This function calculates the average degree of protein in the graph. 
    The average degree is the sum of all the degrees divided by the number of proteins.

    Parameters
    ----------
    file : TextIOWrapper
        interactome file

    Returns
    -------
    average_degree_int :int
        the average protein content of the graph.

    """
    interaction_dict = read_interaction_file_dict(file)
    vertices_degree_dict={}# create new dictionnary
    sum_degree=0#initialise sum degree at 0
    for key in interaction_dict.keys() :# for each key
       vertices_degree_dict[key] = [] #create the key of dictionnary
       vertices_degree_dict[key].append(len(interaction_dict[key]))#we recuperate degree for each prot
       number=get_degree(file,key)
       sum_degree+=number# calculate the sum of each degree
    nb_prot_int=len(interaction_dict)# return the number of protein in interactome file
    average_degree_int=sum_degree/nb_prot_int
    print("The average protein content of the graph is : " ,average_degree_int)
    return average_degree_int

def count_degree(file,deg):
    """
    This function counts the number of proteins with a given degree.

    Parameters
    ----------
    file : TextIOWrapper
        interactome
    deg : str
         imput degree 

    Returns
    -------
    number_prot : int
        the number of proteins with a given degree

    """
    interaction_dict = read_interaction_file_dict(file)
    vertices_degree_dict={}
    number_prot=0#initialize number of prot at 0
    # for each vertices/[prot]
    for key in interaction_dict.keys() :
       vertices_degree_dict[key] = []
       vertices_degree_dict[key].append(len(interaction_dict[key]))
       number=get_degree(file,key)
       if number == deg :#if degree of prot is egal at given degree
            number_prot+=1#increase of 1 the number of prot
    #print(" The number of protein with a degree of " ,deg ,"is de" ,number_prot)
    return number_prot   
    
def histogramme_degree(file, dmin,dmax):
    """
    calculates, for all degrees d between dmin and dmax, the number of proteins having a degree d

    Parameters
    ----------
    file : TYPE
        DESCRIPTION.
    dmin : int
        minimal degree
    dmax : int
        maximum degree

    Returns
    -------
    representation of histogramm style .*represent the 1 prot with are degree d

    """
    # add 1 because the firs element are index 0
    for number in range(dmin,dmax+1):
        #calculate the number of protein at degree inclued bettween  dmin and dmax
            number_prot=count_degree(file,number)
            print("%s " %(number),end= '')
            for i in range(0,number_prot):
                print("*", end='')
            print("\n")
#The distribution tends to decrease for high levels, if one takes a particular protein in general it has a low number of protein interactions.


   
   
    
#call the functions

#print(read_interaction_file_dict("toy_example.txt")) 
#print(read_interaction_file_list("toy_example.txt"))
#print(read_interaction_file("toy_example.txt"))
#print(is_interaction_file("toy_example.txt"))    
