#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  5 12:49:33 2020

@author: Vivian Robin
"""
import pickle
import Analysis_Interactome_Python_OOP
import DomainGraph
import unittest
class test_interactome_oop(unittest.TestCase):
  
  
        # All test with toy_example file
        """
        def test_interactome_file_type(self):
            interactome1=Analysis_Interactome_Python_OOP.Interactome("data/toy_example.txt")
            interactome1.is_interaction_file("data/toy_example.txt")# result expected TRUE
            # test with a large file
            interactome2=Analysis_Interactome_Python_OOP.Interactome("data/Human_HighQuality.txt")
            interactome2.is_interaction_file("data/Human_HighQuality.txt")
            self.assertEqual(type(interactome1.int_dict), dict)
            self.assertEqual(type(interactome1.int_list), list)
            self.assertEqual(type(interactome1.proteins), list)
        
       
        def test_calculate_degree(self):
            interactome1=Analysis_Interactome_Python_OOP.Interactome("data/toy_example.txt")
            interactome1.int_dict
            interactome1.int_list
            interactome1.proteins
            interactome1.count_vertices()# result expected 6
            interactome1.count_edges()# result expected 6
            #interactome1.clean_interactome()# the file cleaning is identique because there are not redondance and homodimere
            interactome1.get_degree("A")# result expected 2
            interactome1.get_max_degree()# result expected B,D with max degree equal 3
            interactome1.get_ave_degree()# result expected 2
            interactome1.count_degree(3)#result expected 2
            #interactome1.histogramme_degree(1,4)
            #test with large file
            
            #answer lot of informatic ressources 
            interactome2=Analysis_Interactome_Python_OOP.Interactome("data/Human_HighQuality.txt")
            interactome2.int_dict
            interactome2.int_list
            interactome2.proteins
            interactome2.count_vertices()# result expected 6
            interactome2.count_edges()# result expected 6
            interactome2.clean_interactome()# the file cleaning is identique because there are not redondance and homodimere
            interactome2.get_degree("RNPS1_HUMAN")
            interactome2.get_max_degree()# result expected B,D with max degree equal 3
            interactome2.get_ave_degree()# result expected 2
            #interactome2.count_degree(3)#result expected 2
            #interactome2.histogramme_degree(1,4)
            
        
        
  
        def test_composant_connexes(self): 
            # Test connexe component
            interactome3=Analysis_Interactome_Python_OOP.Interactome("data/CC_file.txt")
            print(interactome3.extractCC("A"))# result expected [A,B,C,D,E]
            print(interactome3.countCC())# result expected 3 cc with respective length 5,2,3
            #interactome3.writeCC()# new file generated
            interactome2=Analysis_Interactome_Python_OOP.Interactome("data/Human_HighQuality.txt")
            print(interactome2.computeCC())class test_interactome_oop(unittest.TestCase):
            print(interactome3.computeCC())# list of cc for all proteins
            print(interactome3.density())
            print(interactome3.clustering("C"))
            # Test Connexe component Large file
            interactome2.extractCC("SIVA_HUMAN")
            interactome3.countCC()# result expected 139 cc 
            #interactome2.writeCC()# new file generated
            #interactome2.computeCC()# list of cc for all proteins
            print(interactome2.density())
            interactome2.clustering("P85A_HUMAN")
           
        def test_write_proteins_list(self):
            interactome2=Analysis_Interactome_Python_OOP.Interactome("data/Human_HighQuality.txt")
            interactome2.write_list_protein()
        """   
        def test_proteome(self):
            
            interactome1=Analysis_Interactome_Python_OOP.Interactome("data/Human_HighQuality.txt","data/uniprot-yourlist.tab")    
            proteome_human_global=Analysis_Interactome_Python_OOP.Proteome("data/uniprot-yourlist.tab")
          
            #print(proteome_human_global.proteome_dict)
            print(str(len(interactome1.proteins))+" human protein in interactome\n")#we have 9596 human  protein in interactome
            interactome1.count_edges()#we have 27276 interactions
            print(str(len(proteome_human_global.proteome_dict))+" human protein in proteome\n")# we have 8479 proteinswhich have a uniprot identifier
            
            #print(len(proteome_human_global.proteome_list))
    
            
            #print(proteome_human_global.proteome_list)
            #print(proteome_human_global.uniprot_dict)
            #print(proteome_human_global.uniprot_dict.values())
            i=interactome1.xlinkUniprot("data/uniprot-yourlist.tab")
            #interactome1.xlinkDomains()
            
            #print(interactome1.get_protein_domains("INSR_HUMAN"))
            
            print(str(len(interactome1.xlinkUniprot("data/uniprot-yourlist.tab")))+" protein which has a uniprot identifier\n")
            print(str(len(interactome1.proteins)-len(interactome1.xlinkUniprot("data/uniprot-yourlist.tab")))+" proteins from our interactome did not identify uniprot found\n")
            #print(proteome_human_global.proteome_dict["INSR_HUMAN"])
            #print(proteome_human_global.uniprot_dict["P06213"])
            
            #print(interactome1.ls_protein())  
            print(len(interactome1.ls_domains()))
           # interactome1.count_domains_repetive()
            #print(interactome1.ls_domains_n(2))
            #print(interactome1.ls_domains_n(120))
             
            #print(interactome1.nbDomainsByProteinsDistribution())
            #print(interactome1.nbProteinsByDomainDistribution())
          
           # interactome1.hist_nbDomainsByProteinsDistribution()
          #  interactome1.hist_nbProteinsByDomainDistribution()
            
            #print(interactome1.co_occurrence("zf-C2H2","Pkinase"))
            #print(interactome1.co_occurrence("fn3","Pkinase"))
            #print(interactome1.co_occurrence("zf-C2H2","zf-C2H2"))
            #print(interactome1.co_occurrence("Ank_2","Ank_2"))
            
            
            interactome1.hist_nbProteinsByDomainsName()
            
            #print(i["INSR_HUMAN"]['UniprotID'])#result expected P06213
            
            #print(i["INSR_HUMAN"]["Neighbor"])
           # with open('data/prot_neighbor_domain.pickle', 'rb') as domain_dict:
            #    i2=pickle.load(domain_dict)
             #   print(i2["INSR_HUMAN"]['Neighbor'])
              #  print(i2["INSR_HUMAN"]['UniprotID'])
               # print(i2["INSR_HUMAN"]['domains'])
            
            
                    
            #print(interactome1.weighted_cooccurrence_graph())
            #interactome1.export_coocurence_graph_dict()
         
        def test_DomainGraph(self):
            domain_graph=DomainGraph.DomainGraph()
            domain_graph.generate_cooccurrence_graph()
            print(str(domain_graph.nb_vertices)+" number of vertices in domain graph\n")
            print(str(domain_graph.count_edges())+" number of edges in domain graph\n")
            print("\n The density of graph is "+ str( domain_graph.density()))
            print(domain_graph.top_domain_more_neighbors())
            print(domain_graph.top_domain_less_neighbors())
            print(domain_graph.compare_domain_frequencies_more_neighbors())
            print(str(domain_graph.cooccurence_same_domain())+" domains in co-occurrence with themselves \n")
            domain2_graph=DomainGraph.DomainGraph()
           # domain2_graph.generate_cooccurrence_graph_np(3)
            domain2_graph.link_n_density()
            #domain3_graph=DomainGraph.DomainGraph()
           # print(domain3_graph.graph_threshold(10))
            #print(domain3_graph.generate_cooccurrence_graph())
            #domain3_graph.generate_cooccurrence_graph_n(2)
            #domain4=domain3_graph.generate_cooccurrence_graph_weighted()
            #print(domain4.cooccurence_dict_simplified )
           
           

            
if __name__ == '__main__':
    unittest.main()