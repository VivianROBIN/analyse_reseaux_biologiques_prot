# Analysis of biological networks in python

## Installation of the project



```bash
git clone https://gitlab.com/VivianROBIN/analyse_reseaux_biologiques_prot.git
```

**Execution :**

Open a terminal in the folder download by git or via an ide like spyder :

```bash
py Test_Interactome_OOP_Python.py
```

## Organization of the project 



This project is organized in 6 weeks : 

1. Week 1 : Reading a graph interactions between proteins.
2. Week 2 :  Exploration of the protein-protein interaction graph.
3. Week 3 : Changing specifications, object-oriented python.
4. Week 4 : Calculation of the related components of a protein interaction graph.
5. Week 5 : Research of the domains that make up proteins.
6. Week 6 : Compositional analysis in interactome domains.

 The non-object-oriented scripts have been deposited in the directory:  :file_folder: **explorating_interactome_graphe**.

We notice that the non-object-oriented is not optimal, we read the same file over and over again and the memory is quickly saturated.

In the course of our analysis we will focus on the file named: : :page_facing_up: ​**Human_HighQuality.txt** in the **data** folder

### Main results 

- Our interactome is composed of ***9596*** human proteins.
- Our interactome is composed of ***9596*** vertices.
- Our interactome is composed of ***27276*** vertices or interactions.
- The name of the maximum grade protein is ***ATX1_HUMAN*** with a grade of ***207***.
- The average degree of proteins in the graph is ***5.685***.
- Our interaction graph is composed of ***139*** related components.
- The density of the interaction graph is ***0.0005925***.
- On uniprot ***8479*** protein have a correspondence with a human uniprot identifier ( Our proteome).
- On our 9596 protein , ***7412*** we have a uniprot identifier.
- ***2184*** proteins from our interactome have not identifiant uniprot found or no match
- ***3620*** number of vertices in domain graph
- ***2459*** number of edges in domain graph

- The 10 domains with the largest number of neighbours are : ***'PH', 'Pkinase', 'PDZ', 'Ank_2', 'PK_Tyr_Ser-Thr', 'fn3', 'Helicase_C', 'SH3_1', 'C2', 'EGF'***
- The density of  domain graph is ***0.000375***.
- The 10 domains with the smallest number of neighbors : ***M-inducer_phosp, 'Par3_HAL_N_term', 'Na_H_Exchanger', 'NEXCaM_BD', 'OTU', 'HSF_DNA-bind', 'Vert_HS_TF', 'IRS', '6PF2K', 'His_Phos_1***
- ***FALSE***, The domains with the highest number of neighbors are not the ones that are present the most times in the starting proteins.
- ***1 domains*** in co-occurrence with themselves.

