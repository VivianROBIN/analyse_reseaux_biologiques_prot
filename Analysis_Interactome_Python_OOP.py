#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  4 07:35:42 2020

@author: Vivian Robin
"""
import os
import imghdr
import sys

import urllib
import re
import tempfile
import pickle

from collections import Counter
import matplotlib.pyplot as plt
import DomainGraph
import csv

class Interactome :
    
    #Attributes
    int_dict = {}
    int_list = []
    proteins = []
    dict_graphe={}
    count_domains_dict={}
####Constructor
    def __init__(self,file,proteome_file) :
        self.int_list=self.read_interaction_file_list(file)
        self.int_dict=self.read_interaction_file_dict(file)
        self.proteins=self.proteins_list()
        self.dict_graphe=self.xlinkUniprot(proteome_file)
        self.count_domains_dict=self.count_domains_repetive()
        
        #Methods
    def read_interaction_file_dict(self,file):
        """
        Parameters
        ----------
        interaction_file : TextIOWrapper
        This function reads an interaction graph file and returns a dictionary.
        In this dictionary each vertex of the interaction graph and a key, 
        the neighbours of these vertices are the values associated with the keys.
        This function needs an interaction graph file Ex.read_interaction_file_dict("toy_example.txt") 
        Return
        -------
        This function returns a dictionary representing the interaction graph.
        """
        with open(file, "r") as graphe_interaction_file_TextIOWrapper:
            #creation of an empty dictionary
            dico_dict = {}
            #we read the first line of the graph file which corresponds to the number of interactions
            graphe_interaction_file_TextIOWrapper.readline()
            #we read each line one at a time
            for ligne_str in graphe_interaction_file_TextIOWrapper.readlines():
                #Each line is composed of two proteins. We separate the lines according to the character space.
                element_list=ligne_str.split()
                #prot1 corresponds to the first protein of the line
                prot1=element_list[0]
                #prot2 corresponds to the second protein of the line
                prot2=element_list[1]
                #if prot1 is not a key 
                if prot1 not in dico_dict.keys() :
                    #prot1 becomes a key
                    dico_dict[prot1] = []
                    #the second element of the line the value corresponding to the key
                    dico_dict[prot1].append(prot2)
                    #otherwise if prot1 is already a key
                else:
                    #the second element of the line the value corresponding to the key
                    dico_dict[prot1].append(prot2)
                    #if prot2 is not a key
                if prot2 not in dico_dict.keys() :
                    #prot2 becomes a key
                    dico_dict[prot2] = []
                    #the second element of the line the value corresponding to the key
                    dico_dict[prot2].append(prot1)
                    #otherwise if prot2 is already a key
                else:
                    #the second element of the line the value corresponding to the key
                    dico_dict[prot2].append(prot1)   
                    #the dictionary is returned        
        #print(dico_dict)
        return(dico_dict)
    def read_interaction_file_list(self,file):
        """
        Parameters
        ----------
        interaction_file : TextIOWrapper
        This function reads an interaction graph file.
        The output is a list of couples(x,y ) where x represents the first protein of the line 
        and y the second protein of the line.
        Returns
        -------
        This function returns a interaction list representing the interaction graph.
        """
        with open(file, "r") as graphe_interaction_file_TextIOWrapper:
            interaction_couple_list=[]
            #we read the first line of the graph file which corresponds to the number of interactions
            graphe_interaction_file_TextIOWrapper.readline()
            for ligne_str in graphe_interaction_file_TextIOWrapper.readlines():
                #Each line is composed of two proteins. We separate the lines according to the character space.
                element_list=ligne_str.split()
                # each line create a couple
                interaction_couple_list.append((element_list[0],element_list[1]))
            #print(interaction_couple_list)
            #we return a list of each couple
            return(interaction_couple_list)
        
    def proteins_list(self) :
        """ list of proteins present in the graph."""
        proteins_list = []
        for key in self.int_dict.keys() :
            proteins_list.append(key)
        return(proteins_list)
    def __str__(self):
        return(str(self.proteins))
    def nb_ligne(file):
        """
        Parameters
        ----------
        interaction_file : TextIOWrapper
        This function counts the number of lines in the interactions file.
        Returns
        -------
        the total number of line for file_interaction
        """
        #open interaction file
        fd = open(file, 'r')
        # initialization of counter
        n = 0 
        #you go through each line
        for line in fd: 
            #the counter is incremented by 1 for each new line
            n += 1
            #we return the counter from which we subtract one to have that the number of interactions    
        return(n-1)

    def is_interaction_file(self,file):
        """
        Parameters
        ----------
        interaction_file : TextIOWrapper
        this function makes it possible to see if our input file is a good interaction file
        Returns
        -------
        the result answering the question is an interaction file? 
        if the answer is false it is not an interaction file 
        otherwise it is an interaction file so TRUE
        """
        #we test if the file is empty :
        if (os.path.getsize(file) == 0 ):
                print(False)#if the file is empty, we print that it is not an interaction file  stop the program.
                sys.exit()#stop the program.
            #we test if the input file is not an image
        if (imghdr.what(file) =='rgb' or imghdr.what(file)=='gif' or 
                imghdr.what(file)=='pbm' or imghdr.what(file)=='pgm'or 
                imghdr.what(file)=='ppm' or imghdr.what(file)=='tiff' or
                imghdr.what(file)=='rast' or imghdr.what(file)=='xbm' or 
                imghdr.what(file)=='jpeg'or imghdr.what(file)=='bmp' or 
                imghdr.what(file)=='png'):
                print(False) #if the imput file is pictures we print that it is not an interaction file  
                sys.exit() # stop the program.
                #open the file representing the interactions graph
        with open(file, "r") as graphe_interaction_file_TextIOWrapper:
                #the first line is the interation number
                nb_interaction_str = graphe_interaction_file_TextIOWrapper.readline()
                #we compare if the interaction number is the same as the line number
                try:#we test whether the conversion of the first line of interaction is possible
                    nb_interaction_int=int(nb_interaction_str)
                    if(Interactome.nb_ligne(file)!= nb_interaction_int):
                        print(False)#if this number is different it is not a file representing an interaction graph.
                        sys.exit()#stop the program
                    #if conversion is not possible
                except:
                    print(False)#the file does not contain the first line the number of interactions it is not an interaction file
                    sys.exit()#stop the program
                else :
                    for ligne_str in graphe_interaction_file_TextIOWrapper.readlines():
                        #Each line is composed of two proteins. 
                        #We separate the lines according to the character space.
                        element_list=ligne_str.split()
                        #we check that the file has the right number of columns, i.e. two columns for each line.
                        if(len(element_list)>2 or len(element_list)<2):
                            print(False)#if a line has more than 2 columns or less than 2 columns, it is not an interaction file.
                            sys.exit()
        print(True)#the input file is indeed an interaction file

    
############################### WEEK 2 #############################
# Explorating  Interactome Graphe ##################################
####################################################################
    def count_vertices(self):
        """
        Returns
        -------
        nb_vertices_int : int
        the number of vertices

        """
        #the number of vertices in an interaction file corresponds to the number of keys
        #we return the number of key
        nb_vertices_int=len(self.int_dict)
        #the number of vertices is displayed
        print("Number of vertices : ", nb_vertices_int)
        return nb_vertices_int
    def count_edges(self):
        """
        

        Returns
        -------
        nb_edges_int : int
        the number of edges

        """
        #the number of edges corresponds to the number of interaction or, 
        #in a list, a line corresponds to an interaction.
        nb_edges_int=len(self.int_list)
        print("Number of edges: "+str( nb_edges_int) +"\n")
        return nb_edges_int
    
    def clean_interactome(self) :
        list_interactions = []
        for interaction_couple in self.int_list  :
            interaction_couple_2 = (interaction_couple[1], interaction_couple[0])
            # control redondance
            if interaction_couple not in list_interactions :
                if interaction_couple_2 not in list_interactions :
                    #control homodimeric interaction
                    if interaction_couple[0] != interaction_couple[1]:
                        list_interactions.append(interaction_couple)
        number_interactions = str(len(list_interactions))
        #recuperate the nane of file 
        #name_file=self.split(".")
        #create new interactome file 
        with open("Interactome_Cleaning.txt", "w") as interatome_file_cleaning:
            #write the first line 
            interatome_file_cleaning.write(str(number_interactions + '\n'))
            for interaction_couple in list_interactions :
                new_interaction = interaction_couple[0], interaction_couple[1]
                #each prot at separate by space
                new_interaction_bis = ' '.join(new_interaction)
                #at the end of each line a return to the line is added
                interatome_file_cleaning.write(str(new_interaction_bis + '\n'))
#Calculate degree
    def get_degree(self, prot) :
        """
        Parameters
        ----------
        prot : str
            name of protein

        Returns
        -------
        number_interactions : int
            The degree interaction of protein mentionned

        """
        for protein in self.int_dict.keys() :
            #verify imput prot is a keys 
            if prot == protein :
                #mesure number of values of keys
                number_interactions = len(self.int_dict[protein])
                #print("Le degres de la protéine" ,prot , "est de " , number_interactions)
        return number_interactions
    
    def get_max_degree(self) :
        """
        Returns
        -------
        result : list
             The name of prot which the max degree and max degree
        """
        vertices_degree_dict={}
        for key in self.int_dict.keys()  :
            #create new dictionnary
            vertices_degree_dict[key] = []
            #key is prot , value is degree
            vertices_degree_dict[key].append(len(self.int_dict[key]))  
            #create a list with unique prot with max degree
            key_list = [k  for (k, val) in vertices_degree_dict.items() if val== max(vertices_degree_dict.values())]
            # the result is composed prot whitch max degree , and max degree
        result=key_list, max(vertices_degree_dict.values())
        #print(result)
        return result
    
    def get_ave_degree(self):
        """
        

        Returns
        -------
        average_degree_int : int
        The average protein content of the graph

        """
        degrees = []
        for protein in self.proteins :
            degrees.append(self.get_degree(protein))
        vertices_nb = self.count_vertices()
        degree_sum = sum(degrees)
        average_degree_int = degree_sum / vertices_nb
        #print("The average protein content of the graph is : " ,average_degree_int)
        return(average_degree_int)
    
    def count_degree(self,deg):
        """
        

        Parameters
        ----------
        deg : str
            imput degree

        Returns
        -------
        number_prot : int
            the number of proteins with a given degree

        """
        vertices_degree_dict={}
        number_prot=0#initialize number of prot at 0
        # for each vertices/[prot]
        for key in self.int_dict.keys() :
            vertices_degree_dict[key] = []
            vertices_degree_dict[key].append(len(self.int_dict[key]))
            number=self.get_degree(key)
            #if degree of prot is egal at given degree
            if number == deg :
                number_prot+=1#increase of 1 the number of prot
        #print(" The number of protein with a degree of " ,deg ,"is de" ,number_prot)
        return number_prot   

    def histogramme_degree(self, dmin,dmax):
        # add 1 because the firs element are index 0
        for number_int in range(dmin,dmax+1):
            #calculate the number of protein at degree inclued bettween  dmin and dmax
            number_prot_int=self.count_degree(number_int)
            print("%s " %(number_int),end= '')
            for i in range(0,number_prot_int):
                print("*", end='')
            print("\n")
######## WEEk 4 ##################################
######related components##########################
##################################################
    def extractCC(self,prot):
        '''
        

        Parameters
        ----------
        prot : character
           name of proteins

        Returns
        -------
        cc_list : list
            The related component of the input signal is returned.

        '''
        
        cc_list = [prot]
        for key in cc_list :
            #we look to see if the protein entering is indeed a protein of our graphic
            if prot not in self.proteins:
                    print(" protein not existing in interactome file")
            else:
                
                for prot_cc in self.int_dict[key]:
                    # we look at the neighbour of this protein
                    if prot_cc not in cc_list :
                        #we add the neighbour to the cc
                        cc_list.append(prot_cc)
                
        return cc_list
    
    def countCC(self):
        """
        

        Returns
        -------
        cc_list : list cc
            This list contains two elements per tuple: 
                the name of the component and how many proteins it is composed of.

        """
        prot_list = list(self.int_dict.keys())
        cc_list = []
        #one initializes a counter
        num_cc=1
        #as long as he has protein to go through
        while len(prot_list) != 0 :
            #all the cc's are recovered
             current_cc_list = self.extractCC(prot_list[0])
             for prot_marque in current_cc_list:
                 #we remove the already marked proteins already belonging to a cc
                 prot_list.remove(prot_marque)
             cc_index= "CC_" +str(num_cc)#the components are named according to order of appearance
             cc_list.append((cc_index, len(current_cc_list)))
             num_cc+=1  

        return cc_list
   
    def writeCC(self):
        """
        

        Returns
        -------
        list_CC_without_save : list
            a list of three elements is returned: 
                the first is the name of the related component,
                the second is the number of prot that compose it
                and the third is the name of the prot that compose a related component.

        """
        #all the cc's are retrieved
        cc_list = self.countCC()
        prot_list = list(self.int_dict.keys())
        list_CC_without_save=[]
        #file where all the cc's will be returned
        with open("List_CC.txt", "w") as list_CC:
             for tuple_iterator in cc_list:
                 #1st  elementof each line the name of the cc
                 cc_name=tuple_iterator[0]
                 #2nd element of each line the size of the cc
                 cc_length_int = tuple_iterator[1]
                 #33rd element of each line are the proteins that make up the cc
                 current_cc_list = self.extractCC(prot_list[0])
                 for prot_marque in current_cc_list:
                     prot_list.remove(prot_marque)
                     #we write the file returns
                 list_CC.write("{}\t{} \t {}\n".format(cc_name,cc_length_int, current_cc_list))
                 list_CC_without_save.append((cc_name,cc_length_int,current_cc_list))
         
        return list_CC_without_save
     
    def computeCC(self):
        """
        

        Returns
        -------
        cc_protein_index_list : list
           lcc list in which each lcc[i]element corresponds
           to the related component number of the protein at the positionidin the protein list in the graph

        """
        #we recover the set of components when cutting and the proteins that compose it.
        cc_list = self.writeCC()
        cc_protein_index_list = []
        
        for iterateur in cc_list : 
                for i in iterateur[2]:
                    #we look to see if the protein is in the component
                    if i  in iterateur[2]:
                        #the component name is separated according to _.
                         num_cc_int=iterateur[0].split("_")#this way the cc number of each protein can be retrieved.
                         cc_protein_index_list.append(num_cc_int[1])
        return cc_protein_index_list
    
    
    def density(self):
        """
        

        Returns
        -------
        density_float : float
            density of the interactome object

        """
        # recuperate all edges
        existing_edges=2*self.count_edges()
        theorical_edges=len(self.proteins) * (len(self.proteins)-1)
        density_float=existing_edges/theorical_edges
        return density_float

        


    def  clustering(self,prot):
        """
        

        Parameters
        ----------
        prot : Str
            the name of a protein in the graph

        Returns
        -------
        cluster_coef : float
           The clustering coefficient in a graph (also called the clustering, connection, clustering, aggregation or transitivity coefficient), 
           is a measure of the clustering of nodes in a network.

        """
        degree_int = self.get_degree(prot)
        #initialise number of triangle
        nb_triangle_int= 0
        # see the all neighbor
        pair_of_neighbor_float = (len(self.int_dict[prot])*(len(self.int_dict[prot])-1))/2
        if degree_int > 1:
            for neighbor in self.int_dict[prot]:
                for neighbor_of_neighbor in self.int_dict[neighbor]:
                    # look of the neighbor of protein create a new triangle
                    if neighbor_of_neighbor in self.int_dict[prot]:
                        nb_triangle_int += 1
                        cluster_coef_float= (nb_triangle_int/pair_of_neighbor_float)/2
                      
          
        return cluster_coef_float
    
    def write_list_protein(self):
        """
       this function allows the recording of all the names of our protein interactome

        Returns
        -------
        a txt  file named "Protein_List.txt "
        """
        protein_list = self.proteins
        with open("Protein_List.txt", "w") as protein_list_file:
           for protein in protein_list:
               protein_list_file.write("{}\n".format(protein))
    
#we will be interested in the Human proteome and interactome.    
    def xlinkUniprot(self,proteome_file):
        """
        

        Parameters
        ----------
         proteome_file : .tab file
            A tabular file with 5 columns for uniprot.
            Entry	Protein names	Gene names	Length	Entry name

        Returns
        -------
        graph_dict :dict
            each protein of the interactome will have its name, its uniprot identifier and its neighbours.
        """
        graph_dict={}
        id_dict = Proteome(proteome_file)
        for key in self.int_dict.keys():
            if key in id_dict.proteome_dict.keys():
                graph_dict[key]={}
                graph_dict[key]['UniprotID'] = id_dict.proteome_dict[key]
                graph_dict[key]["Neighbor"] = self.int_dict[key]
        
        return (graph_dict)

    
        
    
    def get_protein_domains(self,p):
        '''
        

        Parameters
        ----------
        p : str
            the uniprot identifier of a protein

        Returns
        -------
        domain_list : list 
            a list of protein domains

        '''
        url_str = "https://www.uniprot.org/uniprot/{}.txt".format(p)
        with urllib.request.urlopen(url_str) as rep_request:
            #create a temp file we can contain result of request
            with tempfile.TemporaryFile() as tmp_file:
                tmp_file.write(rep_request.read())
                # we found all document (argument 0)
                tmp_file.seek(0)
                #we are looking for the lines that have this regular expression
                domains_list = re.findall(r'DR\s{3}Pfam; \w+; [\w\-]+; \d+', str(tmp_file.read()))
                domains_dict = {}
                domains_final_list=[]
                for domain in domains_list:
                   domain_properties = domain.split("; ")
                   domains_dict[domain_properties[2]]=int(domain_properties[3])
                for key, value in domains_dict.items():
                    #Domains that are repeated in a protein are highlighted.
                    domains_final_list.append([key]*value)
                return(domains_final_list)
                tmp_file.close()
                
                
                

        
    def xlinkDomains(self):
        """
        

        Returns
        -------
        dict_graphe : dict
            each protein of the interactome, will have its name, its uniprot identifier and its neighbours as well as the domains in a dictionary.

        """
        dict_graphe=self.dict_graphe
        with open('data/prot_neighbor_domain_all.pickle', 'wb') as f:
            for key in self.dict_graphe.keys():
                #For each protein, search the protein domain in pfam
                self.dict_graphe[key]["domains"] = self.get_protein_domains(key)
                
            
        pickle.dump(dict_graphe, f )
        #return dict_graphe
        
    def ls_protein(self):
        '''
        

        Returns
        -------
        prot_list : liste
            the non-redundant list of all proteins found in the protein-protein interaction plot

        '''
        with open('data/prot_neighbor_domain.pickle', 'rb') as domain_dict:
            #Initialization list
            prot_list=[]
            # load the global dictionnary with name_prot , id uniprot , neighbors and domains
            proteome_dict=pickle.load(domain_dict)
            #recuperate the all keys of dictionary whitch correspond the name of protein
            prot_list.append(proteome_dict.keys())
            return prot_list
        
    def ls_domains(self):
        '''
        

        Returns
        -------
        domains_list : list
             the non-redundant list of all the domains found in the proteins that make up the protein-protein interaction graph
        '''
        with open('data/prot_neighbor_domain.pickle', 'rb') as domain_dict:
             domains_list=[]
             # load the global dictionnary with name_prot , id uniprot , neighbors and domains
             proteome_dict=pickle.load(domain_dict)
             # search for each protein 
             for protein in proteome_dict.keys():
                 domains=proteome_dict[protein]["domains"]
                 for domain in domains:
                     if domain not in domains_list:
                         # add the domain to the domains list 
                         domains_list.append(domain)
                         # remove empty domains
                     if  not domains:
                        del domains_list[domains_list.index(domains)]
        return domains_list

    
    def count_domains_repetive(self):
        
        '''
        Returns
        -------
        count_domains_dict : dict
            a dictionary with in key the name of a domain and in value how many times this domain appears in our interactome

        '''
        with open('data/prot_neighbor_domain.pickle', 'rb') as domain_dict:
             domains_list=[]
             # load the global dictionnary with name_prot , id uniprot , neighbors and domains
             proteome_dict=pickle.load(domain_dict)
             # search for each protein 
             for protein in proteome_dict.keys():
                 domains=proteome_dict[protein]["domains"]
                 for domain in domains:
                     domains_list.append(domain)
                     count_domains_dict = Counter(domains_list)
        #generate a dictionnary in key : the name domain , value the number time is repeated
        filehandler = open("data/Domain_Repeated.pickle","wb")             
        pickle.dump(count_domains_dict,filehandler)
        
          
    def ls_domains_n(self,n):
        '''
        

        Parameters
        ----------
        n : int
            a number representing how many times a domain appears

        Returns
        -------
        domains_repetive_list :list
            a list containing the domains that appear n times or more than n times in our interactome

        '''
        with open('data/Domain_Repeated.pickle', 'rb') as domain_repeat:
            domains_repetive_dict=pickle.load(domain_repeat)
            #Initialize list
            domains_repetive_list=[]
            for key,value in domains_repetive_dict.items():
                if value >= n: 
                    domains_repetive_list.append(key)

            return domains_repetive_list

  
   

        
                
    def nbDomainsByProteinsDistribution(self):
        '''
        This function generates a dictionary in key the number of domain and in values how many protein has this number of domain.

        Returns
        -------
        Dict
            Protein distribution according to the number of domains

        '''
        domain_by_protein_dict={}
        nb_domains=[]
        with open('data/prot_neighbor_domain_non_repeat.pickle', 'rb') as domain_dict:
             # load the global dictionnary with name_prot , id uniprot , neighbors and domains
             proteome_dict=pickle.load(domain_dict)
             for protein in proteome_dict.keys():
                nb_domains.append(len(proteome_dict[protein]["domains"]))
                count_dict = Counter(nb_domains)
                domain_by_protein_dict=count_dict
         #sort the dictionnary from the smallest to the largest number of domains   
        domain_by_protein_dict=sorted(domain_by_protein_dict.items(), key=lambda t: t[0])
        return dict(domain_by_protein_dict)
       
    def hist_nbDomainsByProteinsDistribution(self):
        '''
        This function generates a histogram of distribution in abscissa the number of domains and in ordinate how many proteins have this number of domains.

        Returns
        -------
        histogramme of distribution in number of domains in function in number protein

        '''
        domain_by_protein_dic=self.nbDomainsByProteinsDistribution()
        
        plt.bar(list(domain_by_protein_dic.keys()), domain_by_protein_dic.values(), color='g')
        plt.title("Distribution in number of domains in function in number protein")
        plt.xlabel("Number of Domains ")
        plt.ylabel("Number of Proteins")
        plt.show()
        
     
        
    def nbProteinsByDomainDistribution(self):
        '''
        This function generates a dictionary in key the number of protein and in values how many domains has this number of proteins.

        Returns
        -------
        prot_bydomain_dict : dict
            Domain Distribution accordinf of the number of proteins

        '''
        prot_by_domain_dict={}
        domain_by_prot_dict=self.nbDomainsByProteinsDistribution()
        for keys in domain_by_prot_dict.keys():
            prot_by_domain_dict[domain_by_prot_dict[keys]]=keys
        
        return dict(prot_by_domain_dict)
    
    
    def hist_nbProteinsByDomainDistribution(self):
        '''
        This function generates a histogram of distribution in abscissa the number of proteins and in ordinate how many domains have this number of proteins.

        Returns
        -------
       histogramme of distribution in number of proteins in function in number domain

        '''
        protein_by_domains_dic=self.nbProteinsByDomainDistribution()
        plt.bar(list(protein_by_domains_dic.keys()), protein_by_domains_dic.values(), color='g')
        plt.title("Distribution in number of proteins in function in number domains")
        plt.xlabel("Number of Proteins")
        plt.ylabel("Number of Domains")
        plt.show()
        
        
#I think it's interesting to obtain a graphical representation of the most present domain names.
            
    def hist_nbProteinsByDomainsName(self) :
        '''

        Returns
        -------
         a histogram that shows the name of the most present domains and in how many proteins they are in. 

        '''
        with open('data/Domain_Repeated.pickle', 'rb') as domain_repeat:
            domain_name_by_prot=pickle.load(domain_repeat)
            x=[]
            y=[]
            for key , value in domain_name_by_prot.items():
                if value >120:
                    #recuperate domain name
                    x.append(key)
                    # recuperate number of proteins which appear domain
                    y.append(value)
            plt.bar(x,y)
            plt.ylabel("Number of proteins")
            plt.xlabel("Domain name")
            plt.xticks(rotation=90)
            plt.title("Representation of the most present domains according to the number of protein where it appears")
            plt.show()
       
           
#6.2.6
    def co_occurrence(self,dom_x,dom_y):
        '''
        Count the number of co-occurence of the domain x (dom_x) and the domain y (dom_y) in the Proteome

        Parameters
        ----------
        dom_x : str
            the name of domains x
        dom_y : str
            the name of domains y

        Returns
        -------
        count_occ_int : the number of co-occurence 

        '''
        count_occ_int=int(0)
        with open('data/prot_neighbor_domain.pickle', 'rb') as domain_dict:
             # load the global dictionnary with name_prot , id uniprot , neighbors and domains
             proteome_dict=pickle.load(domain_dict)
        
             for protein in proteome_dict.values():
                 if dom_x in protein.get('domains') and dom_y in protein.get('domains'):
                     count_occ_int+=1
        return count_occ_int

        
    def weighted_cooccurrence_graph(self):
        '''
        allows to have a co-occurrence dictionary
        Returns
        -------
        a dictionary of co-occurrence of domains the keys are the domains the values are the domains with 
        which they have co-occurrence as well as the number of co-occurrence for the protein

        '''
        domain_graph = DomainGraph.DomainGraph()
        dom_graph =  domain_graph.generate_cooccurrence_graph_weighted()
        return(dom_graph.cooccurence_dict_simplified)


    def export_coocurence_graph_dict(self):
         graph_dict = self.weighted_cooccurrence_graph()
         with open("data/Graph_Domain_n120.csv",'w') as file_domain:
             fieldName=["node1","node2", "weight"]
             write = csv.writer(file_domain)
             write.writerow(fieldName)

             for key in graph_dict.keys():
                 node1=key
                 for dom in graph_dict[key]:
                     node2 = dom[0]
                     weight = str(dom[1])
                     write.writerow([node1,node2,weight])
                 

          
              
              
           
                 
        
        
                     
                   
                 
        
               
                
                
             

######################################################################################################
######## WEEK 5 Search Domains which compose Protein #################################################


#A pfam file contains 1 summary indicating (structure of the protein, domains, length, organism) its sequence, its structures, etc. 
#the unitprot identifier of INSR_HUMAN is : P06213 
#The domains of INSR_HUMAN are : Recep_L_domain, Furin-like, Recep_L_domain 3,Insulin_TMD,PK_Tyr_Ser-Thr
        
 #A uniprot page in text format contains the domains after an FT DOMAIN field.                    
class Proteome :
    #Attributes
    proteome_list = []
    proteome_dict = {}
    uniprot_dict = {}
    def __init__(self,proteome_file) :
        self.proteome_dict=self.read_proteome_file_dict(proteome_file)
        self.uniprot_dict=self.read_uniprot_dic(proteome_file)
        self.proteome_list=self.read_proteome_list()
        
    def read_proteome_file_dict(self,proteome_file):
        '''
        

        Parameters
        ----------
        proteome_file : .tab file
            A tabular file with 5 columns for uniprot.
            Entry	Protein names	Gene names	Length	Entry name


        Returns
        -------
       A dictionary where the keys are the names of the proteins and the values identify them uniprot_id of the protein.

        '''
        with open(proteome_file, "r") as proteom_TextIOWrapper:
            #creation of an empty dictionary
            proteome_dict = {}
            #we read the first line of the graph file which corresponds to the number of interactions
            proteom_TextIOWrapper.readline()
            #we read each line one at a time
            for ligne_str in proteom_TextIOWrapper.readlines():
                element_list=ligne_str.split("\t")
                uniprot_id_str=element_list[0]
                prot_name=element_list[4]
                prot_name=prot_name.split("\n")
                if prot_name[0] not in proteome_dict.keys() :
                        proteome_dict[prot_name[0]] = []
                        proteome_dict[prot_name[0]].append(uniprot_id_str)
                if prot_name[0] in proteome_dict.keys() :
                        proteome_dict[prot_name[0]] = []
                        proteome_dict[prot_name[0]].append(uniprot_id_str)

       
        return(proteome_dict)
        
             
    
        
        
    def read_proteome_list(self) :
        """
        

        Returns
        -------
        proteome_list : list
            the list of protein names of the proteome 

        """
        proteome_list = []
        for key in self.proteome_dict.keys():
           proteome_list.append(key)
        return (proteome_list)
    
    def read_uniprot_dic(self,proteome_file):
        """
        

        Parameters
        ----------
        proteome_file : .tab file
            A tabular file with 5 columns for uniprot.
            Entry	Protein names	Gene names	Length	Entry name
        Returns
        -------
        A dictionary where the keys are the uniprot_id identifiers and the values are the names of the proteins.

        """
        with open(proteome_file, "r") as proteom_TextIOWrapper:
            #creation of an empty dictionary
            uniprot_dict = {}
            #we read the first line of the graph file which corresponds to the number of interactions
            proteom_TextIOWrapper.readline()
            #we read each line one at a time
            for ligne_str in proteom_TextIOWrapper.readlines():
                element_list=ligne_str.split("\t")
                uniprot_id_str=element_list[0]
                prot_name=element_list[4]
                
                if uniprot_id_str not in uniprot_dict.keys() :
                        uniprot_dict[uniprot_id_str] = []
                        uniprot_dict[uniprot_id_str].append(prot_name)
                if uniprot_id_str  in uniprot_dict.keys() :
                        uniprot_dict[uniprot_id_str] = []
                        uniprot_dict[uniprot_id_str].append(prot_name)
                
                        
        return(uniprot_dict)


      
    def __str__(self):
        return(str(self.proteome_list))


        
        
            

    
    
            
        
    
                     
       
        
        
        
        
        
        
       
       
            
      

        
   

       


         
         
            
              
         

    

