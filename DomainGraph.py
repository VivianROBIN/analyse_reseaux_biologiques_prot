#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  5 16:20:07 2021

@author: Vivian Robin
"""

import pickle
from collections import Counter
import copy
import operator
import matplotlib.pyplot as plt
import Analysis_Interactome_Python_OOP
import csv


class DomainGraph :
     #Attributes
     dict_graphe={}
     protein=[]
     domains=[]
     cooccurence_dict_simplified={}
     edges=int

    
     
     ####Constructor
     def __init__(self) :
        self.dict_graphe = self.load_uniprot_dict()
        self.proteins=self.proteins_list()
        self.domains=self.ls_domains()
        self.cooccurence_dict_simplified=self.load_cooccurence_dict_simplified()
        self.edges=self.count_edges()
        self.nb_vertices = len(self.domains)
        
     def load_uniprot_dict(self):
         with open("data/prot_neighbor_domain.pickle", 'rb') as f:
            uniprot_dict = pickle.load(f)
         return uniprot_dict
     
     def __str__(self):
        return(str(self.dict_graphe))

     
     def proteins_list(self) :
        """ list of proteins present in the graph."""
        proteins_list = []
        for key in self.dict_graphe.keys() :
            proteins_list.append(key)
        return(proteins_list)
    
     def ls_domains(self):
        '''
        

        Returns
        -------
        domains_list : list
             the non-redundant list of all the domains found in the proteins that make up the protein-protein interaction graph
        '''
        domains_list=[]
        # load the global dictionnary with name_prot , id uniprot , neighbors and domains
        proteome_dict=self.dict_graphe
        # search for each protein 
        for protein in proteome_dict.keys():
            domains=proteome_dict[protein]["domains"]
            for domain in domains:
                     if domain not in domains_list:
                         # add the domain to the domains list 
                         domains_list.append(domain)
                         # remove empty domains
                     if  not domains:
                        del domains_list[domains_list.index(domains)]
        return domains_list

    
    
        
        

       ##################### Method ################################33 
     def generate_cooccurrence_graph(self):
         '''
         Returns
         -------
          a dictionnary of cooccurence domains for each proteins

         '''
         #initialize cooccurence_Dict
         cooccurence_dict={}
         for protein in self.dict_graphe.values():
                # Store the domains in a list
                domains_list=list(protein.get('domains'))
                # for create co-occurrence we interest protein which are one more domains
                if len(domains_list) > 1 :
                    for domain1 in domains_list :
                        for domain2 in domains_list:
                            #verify if differnt domain and protein have more 1 domains 
                             if domain2 != domain1 and len(domain1) > 0 and len(domain2) > 0 :
                                if domain1 not in cooccurence_dict : cooccurence_dict[domain1]={domain2:0}
                                if domain2 not in cooccurence_dict: cooccurence_dict[domain2]={domain1:0}
                                if domain2 not in cooccurence_dict[domain1] : cooccurence_dict[domain1]={domain2:0}
                                if domain1 not in cooccurence_dict[domain2] : cooccurence_dict[domain2]={domain1:0}
                                cooccurence_dict[domain1][domain2] += 1
                                cooccurence_dict[domain2][domain1] += 1



         return(cooccurence_dict)
        # pickle.dump(cooccurence_dict,open("data/coocurrence_dict.pickle", 'wb'))
         
         
     def load_cooccurence_dict(self) :
         #dict with the number of cooccurence betwen two domains
         with open("data/coocurrence_dict.pickle", 'rb') as f:
            cooocurence_dict = pickle.load(f)
         return cooocurence_dict
     def load_cooccurence_dict_simplified(self):
         #dict with the number of cooccurence betwen two domains
         with open('data/coocurrence_dict_without_nb_occurence.pickle', 'rb') as f:
            cooocurence_dict_simplified = pickle.load(f)
         return cooocurence_dict_simplified
     
     
     def count_edges(self):
         '''
         Count the total number of cooccurence

         Returns
         -------
         nb_edges :int 
          the number of coocurence

         '''
         
         new_dict={}
         ls_tuple=[]
         # remove the number of coocurreence
         cooccurrence_dict=self.load_cooccurence_dict()
         for  key,value in cooccurrence_dict.items():
            new_value=str(value).split("'")[1]
            new_dict[key]=[]
            new_dict[key].append(new_value)
         pickle.dump(new_dict,open("data/coocurrence_dict_without_nb_occurence.pickle", 'wb'))
         for key, value in new_dict.items():
             uplet = (key, value)
             ls_tuple.append(uplet)

         for tuple1 in ls_tuple :
            for tuple2 in ls_tuple :
                #verify if same domains
                if tuple1[0] == tuple2[1] and tuple1[1] == tuple2[0] :
                    ls_tuple.remove(tuple2)
         nb_edges = int(len(ls_tuple))
         return (nb_edges)

          
         
         
        
     def density(self):
        """
        

        Returns
        -------
        density_float : float
            density of the cooccurencegraph

        """
        # recuperate all edges
        existing_edges=2*int(self.count_edges())
        theorical_edges=int(self.nb_vertices) * (int(self.nb_vertices)-1)
        density_float=existing_edges/theorical_edges
        return density_float
         
     def top_domain_more_neighbors(self):
         '''
         Returns
         -------
         dict
            the 10 domains with the largest number of neighbors
         '''
         dict_neighbours = {}
         domain_list = []
         with open('data/domain_graph_dict.pkl', 'rb') as f:
            coocc_dict = pickle.load(f)
            for dom,co_occ_domains in coocc_dict.items():
               dict_neighbours[dom] = len(co_occ_domains)
            dict_neighbours_sort = sorted(dict_neighbours.items(), key=lambda x: x[1], reverse=True)[:10]
            for elem in dict_neighbours_sort: 
                domain_list.append(elem[0])
            return domain_list




                
        
            
     def top_domain_less_neighbors(self):
         '''
         Returns
         -------
         dict
             the 10 domains with the smallest number of neighbors

         '''
        
         dict_neighbours = {}
         domain_list = []
         with open('data/domain_graph_dict.pkl', 'rb') as f:
            coocc_dict = pickle.load(f)
            for dom,co_occ_domains in coocc_dict.items():
               dict_neighbours[dom] = len(co_occ_domains)
            dict_neighbours_sort = sorted(dict_neighbours.items(), key=lambda x: x[1])[:10]
            for elem in dict_neighbours_sort: 
                domain_list.append(elem[0])
            return domain_list
         
#6.2.11
     def compare_domain_frequencies_more_neighbors(self) :
         '''
         Returns
         -------
         The answer to this question: Are the domains with the largest number of neighbors simply those that are present the most times in the starting proteins?

         '''
         
         more_neighbors=self.top_domain_more_neighbors()
         freq_dom = {}
         with open('data/Domain_Repeated.pickle', 'rb') as domain_repeat:
            domain_repetive_dict=pickle.load(domain_repeat)
         #print(list_domain_repetive)
         high = domain_repetive_dict.most_common(10)
         high=dict(high)
         freq_dom =high.keys()
        
         if len(more_neighbors) == len(freq_dom) :
           for i in set(more_neighbors) :
                for j in set(freq_dom) :
                    if i == j :
                        return('TRUE')
                    else :
                        return('FALSE, The domains with the highest number of neighbors are not the ones that are present the most times in the starting proteins.\n ')
   
    
     def co_occurrence(self,dom_x,dom_y):
        '''
        Count the number of co-occurence of the domain x (dom_x) and the domain y (dom_y) in the Proteome

        Parameters
        ----------
        dom_x : str
            the name of domains x
        dom_y : str
            the name of domains y

        Returns
        -------
        count_occ_int : the number of co-occurence 

        '''
        co_occ_int = 0
        for prot in self.proteins :
            if dom_x in self.dict_graphe[prot]["domains"] and dom_y in self.dict_graphe[prot]["domains"] :
                co_occ_int += 1
        return(co_occ_int)

    
    
     def cooccurence_same_domain(self):
         '''
         calculates the number of domains that are in co-occurrence with themselves

         Returns
         -------
         None.

         '''
         for dom in self.domains :
            nb = self.co_occurrence(dom, dom)
         return(nb)
     
     def count_edges_with_file(self,graph):
         '''
         

         Parameters
         ----------
         graph : dict
             a domain co-occurrence dictionary

         Returns
         -------
         the number of co-occurrences

         '''
         ls_tuple = []
         for key in graph.keys() :
            for value in graph[key] :
                uplet = (key, value)
                ls_tuple.append(uplet)
         for tuple1 in ls_tuple :
            for tuple2 in ls_tuple :
                if tuple1[0] == tuple2[1] and tuple1[1] == tuple2[0] :
                    ls_tuple.remove(tuple2)
         nb_edges = len(ls_tuple)
         return(nb_edges)

    
       
     def generate_cooccurrence_graph_np(self, n) :
         '''
         create an instance and return a newDomainGraph object, whose vertices are the domains. There must be an interaction between the verticesx only when these two domains areco-occurring in at least n proteins of the graph.

         Parameters
         ----------
         n : int
             n proteins in the graph..

         Returns
         -------
         new instance

         '''
        
         new_Domain_Graph = DomainGraph()
         #create recursive copy in dict for modyfy during boucle
         dict_graph = copy.deepcopy(self.cooccurence_dict_simplified)
         for key in dict_graph.keys() :
            for values in dict_graph[key] :
                if self.co_occurrence(key, values) < n :
                    dict_graph[key].remove(values)
         keys_trash= []
         for key in dict_graph.keys() :
            if len(dict_graph[key]) == 0 :
                keys_trash.append(key)
         #remove keys not associated in values
         for key in keys_trash :
             del dict_graph[key]
         new_Domain_Graph.cooccurence_dict_simplified = dict_graph
        # modify argument for new instance
         list_dom = []
         for dom in dict_graph.keys() :
            list_dom.append(dom)
         new_Domain_Graph.domains = list_dom
         new_Domain_Graph.edges = new_Domain_Graph.count_edges_with_file(dict_graph)
         new_Domain_Graph.nb_vertices = len(new_Domain_Graph.domains)
         return(new_Domain_Graph)
    
     def link_n_density(self):
         '''
         Returns
         -------
         graphical representation between link n and the density of the graph.
         '''
         x=[]
         y=[]
         for n in range(0,11) :
                #recuperate lis of n
                x.append(n)
                new_Graph=self.generate_cooccurrence_graph_np(n)
                density=new_Graph.density()
                # recuperate number of proteins have coocurence
                y.append(density)
         plt.plot(x,y)
         plt.ylabel("density")
         plt.xlabel("number protein have cooccurence")
         plt.title("Link between n and the density of the graph")
         plt.show()


#6.2.15
     def generate_cooccurrence_graph_n(self,n):
         '''
          look for an interaction between vertices x and y only when these two domains co-occur at least n times in the proteins of the graph.

         Parameters
         ----------
         n : int 
             number of cooccurence minimal of domains

         Returns
         -------
         new instance which not depend of number of protein but number of co-occurence

         '''
         #initialise new instance
         new_Domain_Graph = DomainGraph()
         #create recursive copy in dict for modyfy during boucle
         dict_graph = copy.deepcopy(self.cooccurence_dict_simplified)
         for key in dict_graph.keys() :
            for values in dict_graph[key] :
                #conditions is modified
                if self.co_occurrence(key, values) >= n :
                    dict_graph[key].remove(values)
         keys_trash= []
         for key in dict_graph.keys() :
             # we look if key is associated in values
            if len(dict_graph[key]) == 0 :
                keys_trash.append(key)
        #remove keys not associated in values
         for key in keys_trash :
             del dict_graph[key]
         #save the new dictionnary
         new_Domain_Graph.cooccurence_dict_simplified = dict_graph
        # modify argument for new instance
         list_dom = []
         for dom in dict_graph.keys() :
            list_dom.append(dom)
         new_Domain_Graph.domains = list_dom
         new_Domain_Graph.edges = new_Domain_Graph.count_edges_with_file(dict_graph)
         new_Domain_Graph.nb_vertices = len(new_Domain_Graph.domains)
         return(new_Domain_Graph)
     
     def generate_cooccurrence_graph_weighted(self):
         '''
         Create a new instance , We call a weighted graph a graph with vertices and edges, but this time the edges each have a (strictly positive) weight.

         Returns
         -------
         anew instance of Domain Graph

         '''
         new_graph=self.generate_cooccurrence_graph_n(2000)
         coo_graph_weighted_dict={}
         for domain1 in new_graph.cooccurence_dict_simplified.keys() :
                        if domain1 not in coo_graph_weighted_dict.keys():
                            coo_graph_weighted_dict[domain1] = []

                        for domain2  in new_graph.cooccurence_dict_simplified[domain1]:
                             co_occ = self.co_occurrence(domain1, domain2)
                             value = (domain2, co_occ)
                             coo_graph_weighted_dict[domain1].append(value)
         new_graph.cooccurence_dict_simplified = coo_graph_weighted_dict
         #generate new argument for new instance
         list_dom = []
         for dom in coo_graph_weighted_dict.keys() :
            list_dom.append(dom)
         new_graph.domains = list_dom
         new_graph.edges = new_graph.count_edges_with_file(coo_graph_weighted_dict)
         new_graph.nb_vertices = len(new_graph.domains)
         return(new_graph)
     
     def graph_threshold(self,k):
         '''
         

         generates a new graph of domains with a threshold at k. 
         ----------
         k : int
             threshold

         Returns
         -------
         None.

         '''
         interac=Analysis_Interactome_Python_OOP.Interactome("data/Human_HighQuality.txt","data/uniprot-yourlist.tab") 
         domain_graph_dict=interac.weighted_cooccurrence_graph()
         #print(str(len(domain_graph_dict))+" tatata")
         with open("data/Graph_Domain_n120_threshold10.csv",'w') as file_domain_threshold:
             fieldName=["node1","node2", "weight"]
             write = csv.writer(file_domain_threshold)
             write.writerow(fieldName)
             for key in domain_graph_dict.keys():
                 node1=key
                 for dom in domain_graph_dict[key]:
                     node2 = dom[0]
                     weight = int(dom[1])
                     
                     
                     if weight > k:
                         write.writerow([node1,node2,weight])
         with open("data/Graph_Domain_n120_threshold10.csv",'r') as file_domain:
             linecount = len(file_domain.readlines())
         print(str(len(domain_graph_dict)-linecount -1) +" domain associations are supported by a weighting of less than 10 \n ")
                         
                    
                        
        
        
        
    